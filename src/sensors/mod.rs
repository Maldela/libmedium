//! Module containing the sensors and their functionality.

#[cfg(feature = "sync")]
pub mod sync_sensors;

#[cfg(feature = "async")]
pub mod async_sensors;

mod error;
mod subfunction_type;

use std::path::{Path, PathBuf};

pub use error::Error;
pub use subfunction_type::SensorSubFunctionType;

/// Base trait that all sensors must implement.
/// It contains the functionality to get a sensor's base, index or supported subfunctions.
pub trait Sensor {
    /// Returns this sensor's base like "temp" or "fan".
    fn static_base() -> &'static str where Self: Sized;

    /// Returns this sensor's base like "temp" or "fan".
    fn base(&self) -> &'static str;

    /// Returns this sensor's index.
    fn index(&self) -> u16;

    /// Returns this sensor's hwmon's path.
    fn hwmon_path(&self) -> &Path;

    /// Returns a list of all readable subfunction types supported by this sensor.
    fn supported_read_sub_functions(&self) -> Vec<SensorSubFunctionType> {
        SensorSubFunctionType::read_list()
            .filter(|&s| {
                std::fs::OpenOptions::new()
                    .read(true)
                    .open(self.subfunction_path(s))
                    .is_ok()
            })
            .collect()
    }

    /// Returns the path this sensor's subfunction of the given type would have.
    fn subfunction_path(&self, sub_type: SensorSubFunctionType) -> PathBuf {
        self.hwmon_path().join(format!(
            "{}{}{}",
            self.base(),
            self.index(),
            sub_type.to_suffix()
        ))
    }
}

/// A struct that represents the state of all writeable subfunctions of a sensor.
/// It can be used to reset a sensor to a previous state or copy its settings to another sensor.
#[derive(Debug, Clone, PartialEq)]
pub struct SensorState {
    pub(crate) states: std::collections::HashMap<SensorSubFunctionType, String>,
}

impl SensorState {
    /// Returns an iterator over all subfunction types that this state contains.
    pub fn sub_types(&self) -> impl Iterator<Item = SensorSubFunctionType> + '_ {
        self.states.keys().copied()
    }

    /// Returns the value of a given `SensorSubFunctionType` if present in this `SensorState`
    pub fn value(&self, sub_type: SensorSubFunctionType) -> Option<&str> {
        self.states.get(&sub_type).map(String::as_str)
    }
}
