use super::*;
use crate::hwmon::sync_hwmon::Hwmons;
use crate::sensors::sync_sensors::{fan::*, temp::*};
use crate::tests::*;

use temp_dir::TempDir;

#[test]
fn test_sensor_read_value() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "system", None)
        .add_temp(1, &[("_input", "40000"), ("_label", "temp1")])
        .add_fan(1, &[("_input", "60")]);

    let hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let hwmon = hwmons.hwmon_by_index(0).unwrap();
    let temp = hwmon.temp(1).unwrap();
    let fan = hwmon.fan(1).unwrap();

    #[cfg(not(feature = "uom_units"))]
    assert_eq!(40.0, temp.read_input().unwrap().as_degrees_celsius());

    #[cfg(feature = "uom_units")]
    assert_eq!(
        40.0,
        temp.read_input()
            .unwrap()
            .round::<uom::si::thermodynamic_temperature::degree_celsius>()
            .get::<uom::si::thermodynamic_temperature::degree_celsius>()
    );

    #[cfg(not(feature = "uom_units"))]
    assert_eq!(60, fan.read_input().unwrap().as_rpm());

    #[cfg(feature = "uom_units")]
    assert_eq!(
        60.0,
        fan.read_input()
            .unwrap()
            .round::<uom::si::angular_velocity::revolution_per_minute>()
            .get::<uom::si::angular_velocity::revolution_per_minute>()
    );
}

#[test]
fn test_label() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "system", None)
        .add_temp(1, &[("_input", "40000"), ("_label", "test_temp1")]);

    let hwmons: Hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let hwmon = hwmons.hwmon_by_index(0).unwrap();
    let temp = hwmon.temp(1).unwrap();

    assert_eq!(temp.name(), String::from("test_temp1"));
}

#[test]
#[cfg(feature = "writeable")]
fn test_state() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "system", None)
        .add_temp(
            1,
            &[
                ("_input", "40000"),
                ("_label", "test_temp1"),
                ("_min", "100"),
            ],
        )
        .add_temp(
            2,
            &[
                ("_input", "40000"),
                ("_label", "test_temp2"),
                ("_min", "100"),
                ("_max", "120"),
            ],
        );

    let hwmons: Hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let hwmon = hwmons.hwmon_by_index(0).unwrap();
    let temp = hwmon.writeable_temp(1).unwrap();
    let mut state = temp.state().unwrap();

    assert_eq!(
        state.states.get(&SensorSubFunctionType::Min),
        Option::<&String>::Some(&String::from("100"))
    );

    state
        .states
        .insert(SensorSubFunctionType::Max, String::from("200"));

    assert_eq!(temp.write_state(&state).is_err(), true);
    assert_eq!(temp.write_state_lossy(&state).is_ok(), true);

    let temp2 = hwmon.writeable_temp(2).unwrap();

    assert_eq!(temp2.write_state(&state).is_ok(), true);
    assert_eq!(
        temp2
            .state()
            .unwrap()
            .states
            .get(&SensorSubFunctionType::Max)
            .unwrap()
            .as_str(),
        "200"
    );
}

#[test]
#[cfg(feature = "virtual_sensors")]
fn test_virtual() {
    use super::virt::*;
    use crate::units::Current;

    let test_dir = TempDir::new().unwrap();
    let virtual_path = test_dir.path().join("virtual_sensor");
    let _virtual_builder = VirtualSensorBuilder::create(&virtual_path, Some("100"));

    let virtual_sensor = virtual_sensor_from_path::<Current>(&virtual_path).unwrap();

    #[cfg(feature = "uom_units")]
    assert_eq!(
        virtual_sensor
            .read()
            .unwrap()
            .get::<uom::si::electric_current::milliampere>(),
        100.0
    );

    #[cfg(not(feature = "uom_units"))]
    assert_eq!(virtual_sensor.read().unwrap().as_milli_amperes(), 100);

    let custom_virtual_path = test_dir.path().join("virtual_sensor");
    let _custom_virtual_builder = VirtualSensorBuilder::create(&virtual_path, Some("150"));

    let custom_virtual_sensor = custom_virtual_sensor(|| {
        std::fs::read_to_string(&custom_virtual_path).map(|r| Current::from_raw(&r).unwrap())
    });

    #[cfg(feature = "uom_units")]
    assert_eq!(
        custom_virtual_sensor
            .read()
            .unwrap()
            .get::<uom::si::electric_current::milliampere>(),
        150.0
    );

    #[cfg(not(feature = "uom_units"))]
    assert_eq!(
        custom_virtual_sensor.read().unwrap().as_milli_amperes(),
        150
    );
}

#[test]
fn test_trait_object() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "system", None)
        .add_temp(
            1,
            &[
                ("_input", "40000"),
                ("_label", "test_temp1"),
                ("_min", "100"),
            ],
        );

    let hwmons: Hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let hwmon = hwmons.hwmon_by_index(0).unwrap();
    let _temp: Box<&dyn TempSensor> = Box::new(hwmon.temp(1).unwrap());
}