//! Module containing the humidity sensors and their related functionality.

use super::*;
use crate::hwmon::sync_hwmon::Hwmon;
use crate::units::Ratio;

use std::path::{Path, PathBuf};

/// Helper trait that sums up all functionality of a read-only humidity sensor.
pub trait HumiditySensor: SyncSensor + std::fmt::Debug {
    /// Reads whether or not this sensor is enabled.
    /// Returns an error, if the sensor doesn't support the feature.
    fn read_enable(&self) -> Result<bool> {
        let raw = self.read_raw(SensorSubFunctionType::Enable)?;
        bool::from_raw(&raw).map_err(Error::from)
    }

    /// Reads the input subfunction of this sensor.
    /// Returns an error, if this sensor doesn't support the subtype.
    fn read_input(&self) -> Result<Ratio> {
        let raw = self.read_raw(SensorSubFunctionType::Input)?;
        Ratio::from_raw(&raw).map_err(Error::from)
    }
}

/// Struct that represents a read only humidity sensor.
#[derive(Debug, Clone)]
pub(crate) struct HumiditySensorStruct {
    hwmon_path: PathBuf,
    index: u16,
}

impl Sensor for HumiditySensorStruct {
    fn static_base() -> &'static str where Self: Sized {
        "humidity"
    }
    fn base(&self) -> &'static str {
        "humidity"
    }

    fn index(&self) -> u16 {
        self.index
    }

    fn hwmon_path(&self) -> &Path {
        self.hwmon_path.as_path()
    }
}

impl SyncSensor for HumiditySensorStruct {}

impl SyncSensorExt for HumiditySensorStruct {
    fn parse(hwmon: &Hwmon, index: u16) -> Result<Self> {
        let sensor = Self {
            hwmon_path: hwmon.path().to_path_buf(),
            index,
        };

        inspect_sensor(sensor, SensorSubFunctionType::Input)
    }
}

impl HumiditySensor for HumiditySensorStruct {}

#[cfg(feature = "writeable")]
impl WriteableSensor for HumiditySensorStruct {}

#[cfg(feature = "writeable")]
/// Helper trait that sums up all functionality of a read-write humidity sensor.
pub trait WriteableHumiditySensor: HumiditySensor + WriteableSensor {
    /// Sets this sensor's enabled state.
    /// Returns an error, if the sensor doesn't support the feature.
    fn write_enable(&self, enable: bool) -> Result<()> {
        self.write_raw(SensorSubFunctionType::Enable, &enable.to_raw())
    }
}

#[cfg(feature = "writeable")]
impl WriteableHumiditySensor for HumiditySensorStruct {}
