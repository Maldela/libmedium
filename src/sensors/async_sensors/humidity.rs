//! Module containing the humidity sensors and their related functionality.

use super::*;
use crate::hwmon::async_hwmon::Hwmon;
use crate::units::Ratio;

use std::path::{Path, PathBuf};

#[async_trait]
/// Helper trait that sums up all functionality of a read-only humidity sensor.
pub trait AsyncHumiditySensor: AsyncSensor + std::fmt::Debug {
    /// Reads whether or not this sensor is enabled.
    /// Returns an error, if the sensor doesn't support the feature.
    async fn read_enable(&self) -> Result<bool> {
        let raw = self.read_raw(SensorSubFunctionType::Enable).await?;
        bool::from_raw(&raw).map_err(Error::from)
    }

    /// Reads the input subfunction of this sensor.
    /// Returns an error, if this sensor doesn't support the subtype.
    async fn read_input(&self) -> Result<Ratio> {
        let raw = self.read_raw(SensorSubFunctionType::Input).await?;
        Ratio::from_raw(&raw).map_err(Error::from)
    }
}

/// Struct that represents a read only humidity sensor.
#[derive(Debug, Clone)]
pub(crate) struct HumiditySensorStruct {
    hwmon_path: PathBuf,
    index: u16,
}

impl Sensor for HumiditySensorStruct {
    fn static_base() -> &'static str where Self: Sized {
        "humidity"
    }
    fn base(&self) -> &'static str {
        "humidity"
    }

    fn index(&self) -> u16 {
        self.index
    }

    fn hwmon_path(&self) -> &Path {
        self.hwmon_path.as_path()
    }
}

impl AsyncSensor for HumiditySensorStruct {}

#[async_trait]
impl AsyncSensorExt for HumiditySensorStruct {
    async fn parse(hwmon: &Hwmon, index: u16) -> Result<Self> {
        let sensor = Self {
            hwmon_path: hwmon.path().to_path_buf(),
            index,
        };

        inspect_sensor(sensor, SensorSubFunctionType::Input).await
    }
}

impl AsyncHumiditySensor for HumiditySensorStruct {}

#[cfg(feature = "writeable")]
impl AsyncWriteableSensor for HumiditySensorStruct {}

#[cfg(feature = "writeable")]
#[async_trait]
/// Helper trait that sums up all functionality of a read-write humidity sensor.
pub trait AsyncWriteableHumiditySensor: AsyncHumiditySensor + AsyncWriteableSensor {
    /// Sets this sensor's enabled state.
    /// Returns an error, if the sensor doesn't support the feature.
    async fn write_enable(&self, enable: bool) -> Result<()> {
        self.write_raw(SensorSubFunctionType::Enable, &enable.to_raw())
            .await
    }
}

#[cfg(feature = "writeable")]
impl AsyncWriteableHumiditySensor for HumiditySensorStruct {}
