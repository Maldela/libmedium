//! Module containing the energy sensors and their related functionality.

use super::*;
use crate::hwmon::async_hwmon::Hwmon;
use crate::units::Energy;

use std::path::{Path, PathBuf};

#[async_trait]
/// Helper trait that sums up all functionality of a read-only energy sensor.
pub trait AsyncEnergySensor: AsyncSensor + std::fmt::Debug {
    /// Reads whether or not this sensor is enabled.
    /// Returns an error, if the sensor doesn't support the feature.
    async fn read_enable(&self) -> Result<bool> {
        let raw = self.read_raw(SensorSubFunctionType::Enable).await?;
        bool::from_raw(&raw).map_err(Error::from)
    }

    /// Reads the input subfunction of this sensor.
    /// Returns an error, if this sensor doesn't support the subtype.
    async fn read_input(&self) -> Result<Energy> {
        let raw = self.read_raw(SensorSubFunctionType::Input).await?;
        Energy::from_raw(&raw).map_err(Error::from)
    }
}

#[derive(Debug, Clone)]
pub(crate) struct EnergySensorStruct {
    hwmon_path: PathBuf,
    index: u16,
}

impl Sensor for EnergySensorStruct {
    fn static_base() -> &'static str where Self: Sized {
        "energy"
    }
    fn base(&self) -> &'static str {
        "energy"
    }

    fn index(&self) -> u16 {
        self.index
    }

    fn hwmon_path(&self) -> &Path {
        self.hwmon_path.as_path()
    }
}

impl AsyncSensor for EnergySensorStruct {}

#[async_trait]
impl AsyncSensorExt for EnergySensorStruct {
    async fn parse(hwmon: &Hwmon, index: u16) -> Result<Self> {
        let sensor = Self {
            hwmon_path: hwmon.path().to_path_buf(),
            index,
        };

        inspect_sensor(sensor, SensorSubFunctionType::Input).await
    }
}

impl AsyncEnergySensor for EnergySensorStruct {}

#[cfg(feature = "writeable")]
impl AsyncWriteableSensor for EnergySensorStruct {}

#[cfg(feature = "writeable")]
#[async_trait]
/// Helper trait that sums up all functionality of a read-write energy sensor.
pub trait AsyncWriteableEnergySensor: AsyncEnergySensor + AsyncWriteableSensor {
    /// Sets this sensor's enabled state.
    /// Returns an error, if the sensor doesn't support the feature.
    async fn write_enable(&self, enable: bool) -> Result<()> {
        self.write_raw(SensorSubFunctionType::Enable, &enable.to_raw())
            .await
    }
}

#[cfg(feature = "writeable")]
impl AsyncWriteableEnergySensor for EnergySensorStruct {}
