use super::Hwmons;

use crate::tests::*;
use std::time::Duration;

use temp_dir::TempDir;

#[test]
fn test_hwmon_parse() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "foo", Some("label"));
    VirtualHwmonBuilder::create(test_dir.path(), 1, "bar", None);

    let hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let foo = hwmons.hwmon_by_index(0).unwrap();
    let bar = hwmons.hwmon_by_index(1).unwrap();

    assert_eq!("foo", foo.name());
    assert_eq!(Some("label"), foo.label());
    assert_eq!(Duration::from_secs(1), foo.update_interval().unwrap());
    assert_eq!(test_dir.path().join("hwmon0"), foo.path());
    assert_eq!("bar", bar.name());
    assert_eq!(None, bar.label());
    assert_eq!(test_dir.path().join("hwmon1"), bar.path());
}

#[test]
fn test_hwmon_temps() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "system", None)
        .add_temp(1, &[("_input", "40000"), ("_label", "temp1")])
        .add_temp(2, &[("_input", "60000"), ("_label", "temp2")])
        .add_temp(4, &[("_input", "30000"), ("_label", "temp4")]);

    let hwmons: Hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let hwmon = hwmons.hwmon_by_index(0).unwrap();
    let temps = hwmon.temps();

    assert_eq!(true, temps.get(&1u16).is_some());
    assert_eq!(true, temps.get(&2u16).is_some());
    assert_eq!(true, temps.get(&4u16).is_some());

    assert_eq!(true, temps.get(&3u16).is_none());
}

#[test]
fn test_hwmon_pwms() {
    let test_dir = TempDir::new().unwrap();

    VirtualHwmonBuilder::create(test_dir.path(), 0, "system", None)
        .add_pwm(1, &[("_enable", "1"), ("_mode", "1"), ("", "255")])
        .add_pwm(2, &[("_enable", "1"), ("_mode", "1"), ("", "255")])
        .add_pwm(4, &[("_enable", "1"), ("_mode", "1"), ("", "255")]);

    let hwmons: Hwmons = Hwmons::parse_path(test_dir.path()).unwrap();
    let hwmon = hwmons.hwmon_by_index(0).unwrap();
    let pwms = hwmon.pwms();

    assert_eq!(true, pwms.get(&1u16).is_some());
    assert_eq!(true, pwms.get(&2u16).is_some());
    assert_eq!(true, pwms.get(&4u16).is_some());

    assert_eq!(true, pwms.get(&3u16).is_none());
}
