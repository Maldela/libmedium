mod error;

pub use error::Error;

pub(crate) use error::Result;
